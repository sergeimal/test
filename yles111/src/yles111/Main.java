package yles111;

public class Main {

	public static void main(String[] args) throws Exception {
		//Sozdajom 2 objekta - OK
		Item i1 = new Item("Leib", 0.95);
		Item i2 = new Item("Piim", 0.55);
		
		
		//Sozdajom novqi zakaz(spisok) etih objektov: i1 - 1 shtuka, i2- 5 shtuk - OK
		Order receipt = new Order(); 
		receipt.add(i1, 1); 
		receipt.add(i2, 5); 
	
		
		//Sozdajom novqi sklad 
		Stock sklad = new Stock();
		
		//i dobavlaem metodom "receive" spisok etih objektov, kotorie v "receipt", na sklad. 
		sklad.receive(receipt);
		
		//Delaem novqi zakaz(spisok) i metodom DISPATCH zabiraem eti objekti iz sklada. 
		
		Order receipt2 = new Order(); 
		receipt2.add(i1, 1); 
		receipt2.add(i2, 3); 
		
		sklad.send(receipt2);
		
		
		System.out.println(sklad);
		//Ostalos:
		// 0tk Hleba t.e 1-1=0 i 2tk Piima t.e 5-3=2

		
	}
}
