package yles111;

import yles111.Item;

public class Order {

	private ItemsList items = new ItemsList();

	public void add(Item item, int quantity) {
		items.add(item, quantity);
	}

	@Override
	public String toString() {
		return "Order [items=" + items + "]";
	}
}
