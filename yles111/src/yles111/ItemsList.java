package yles111;

import java.util.HashMap;

import yles111.Item;

public class ItemsList {

	private HashMap<Item, Integer> list = new HashMap<Item, Integer>();

	public void add(Item item, int quantity) {

		list.put(item, quantity);
		
	}

	@Override
	public String toString() {
		return "ItemsList [list=" + list + "]";
	}
}
